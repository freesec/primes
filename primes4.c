/**
* 	primes -- C MPI parallel program to find primes very basically.
*   Author: freesec2021 at protonmail.com
*   Copyright (C) 2019 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/
#include "primes.h"
#include "util.h"

void primes4(uint64_t n_max)
{
	int rank, size;
	uint64_t sqrt_n, proc_n_offset, i, j;
	size_t num_blocks = n_max/sizeof(uint64_t)/8; // 1 bit per number of the list
	uint64_t *primes_masks_llu, *primes_masks_llu2;
	if(n_max&63) num_blocks++;
	primes_masks_llu = malloc(num_blocks*sizeof(uint64_t));
	bzero(primes_masks_llu, num_blocks*sizeof(uint64_t));
	// a 0 means the number is prime (or not yet tested)
	// 1 means the number is composite
	sqrt_n = (uint64_t) ceil(sqrt((double) n_max)); // no need to check past this limit (because of the symmetry in list of dividers)

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	// init each core index in Eratosthenes list
	proc_n_offset = 2 /* when rank == 0 */;
	if(rank > 0)
		proc_n_offset += rank;

	for(i=proc_n_offset;i<sqrt_n;i+=size)
	{

		if(primes_masks_llu[i >> 6] & 1llu << (i&63)) continue;
		for(j=i+1;j<n_max;j++)
		{
			if(! (j & 1) || !(j % i))
			{
				// j has a divider i, remove it from the list
				primes_masks_llu[j >> 6] |= 1llu << (j&63);
			}
		}	
	}

	// allocate the buffer for the MPI reduction
	primes_masks_llu2 = malloc(num_blocks*sizeof(uint64_t));

	// MPI LOR reduction for synchronization 
	// OMPI_DECLSPEC  int MPI_Reduce(const void *sendbuf, void *recvbuf, int count,
	//                              MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm);
	// NOTE: sendbuf and recvbuf must be different memory buffers or MPI ends up with a fatal error (at least that what I noticed on my tests).
	if(rank == 0) printf("Reduction...\n");
	MPI_Reduce(primes_masks_llu, primes_masks_llu2, num_blocks, MPI_UNSIGNED_LONG, MPI_BOR, 0, MPI_COMM_WORLD);

	write_erathostenes_primes("out_primes4", rank, primes_masks_llu2, num_blocks, n_max);

	free(primes_masks_llu);
	free(primes_masks_llu2);
}


