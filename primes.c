/**
* 	primes -- C MPI parallel program to find primes very basically.
*   Author: freesec2021 at protonmail.com
*   Copyright (C) 2019 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/
#include "mpi.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "primes.h"

const unsigned int DEFAULT_MAX_N = 100000;

int is_prime(const uint64_t n)
{
	uint64_t sqrt_n = (uint64_t) ceil(sqrt((double)n)), i = 2;
	for(;i<=sqrt_n; i++)
		if(! (n % i)) return 0;
	return 1;
}

void usage(const char* progname)
{
	printf("%s [<max_n> [<method_index>]]\n", progname);
	printf("The program will search all the primes from 2 to max_n\n");
	printf("There are 5 methods available, check the header primes.h for more information.\n");
	printf("By default, the method used is the first (primes1) and max_n value is %d\n", DEFAULT_MAX_N);
}

int main(int argc, char **argv)
{
	uint64_t max_n = DEFAULT_MAX_N;
	unsigned int method = 0;
	int ret;
	int rank, size;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	if(argc > 1)
	{
		ret = sscanf(argv[1], "%llu", &max_n);
		if(ret != 1) 
		{
			usage(argv[0]);
			return EXIT_FAILURE;
		}
		if(!rank) printf("max_n=%llu\n", max_n);
		if(argc > 2)
		{
			ret = sscanf(argv[2], "%u", &method);
			if(ret != 1) 
			{
				usage(argv[0]);
				return EXIT_FAILURE;
			}

		}
	}
	if(!rank) printf("method=%d\n", method);
	// choose method according to the first argument
	switch(method)
	{
		case 1:
		default:
			primes1(max_n);
			break;
		case 2:
			primes2(max_n);
			break;
		case 3:
			primes3(max_n);
			break;
		case 4:
			primes4(max_n);
			break;
		case 5:
			primes5(max_n);
			break;
			
	}
	MPI_Finalize();
}
