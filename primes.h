/**
* 	primes -- C MPI parallel program to find primes very basically.
*   Author: freesec2021 at protonmail.com
*   Copyright (C) 2019 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/
#ifndef __PRIMES__
#define __PRIMES__

/** This header contains all the methods tested to find prime numbers.
 *  All are very basic methods because the main interest here is to make use of MPI (this use is not really advanced though).
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "mpi.h"
/**
 * First method: spread the set of integers from 2 to max_n to all the cores (of nodes) made available by MPI_COMM_WORLD.
 * For each one of these integers, test all possible dividers starting from 2 (see function is_prime()).
 */
void primes1(const uint64_t max_n);

/**
 * Second method: exactly the same as the first method, except that the absence of synchronization of the output
 * between nodes/cores is handled correctly by writing prime numbers found into separate files (creating one file per MPI rank).
 */
void primes2(uint64_t max_n);

/**
 * Third method: the first difference with primes2 is that here the even integers are skipped (because they can't be primes of course).
 * The second and main difference is the distribution of integers to the MPI cores. The goal is to avoid that one core have to test
 * all the larger numbers (with more possible dividers) by attributing contiguous numbers to different cores along all the set from 2 to max_n
 * (contiguous numbers have comparable number of dividers so the workload balance between cores is acquired).
 */
void primes3(uint64_t max_n);

/**
 * Fourth method: this function tries another approach than previous ones which consists in a distributed version of the Eratosthenes sieve.
 * The Eratosthenes list is binary encoded to make in sort that the ranks of zero bits are the prime numbers.
 * The node core of rank i removes all the multiples of the integers which indices in the list are multiple of i.
 */
void primes4(uint64_t n_max);

/**
 * Fifth method: this function proceeds the same way as the fourth method (distributed Eratosthenes sieve)
 * with a small optimization allowing to decrease the number of operations to modify the Erathostenes list.
 */
void primes5(uint64_t n_max);

/**
 * Returns 1 if no divider has been found for n (starting from 2 to the square root of n).
 * Returns 0 otherwise.
 *
 */
int is_prime(const uint64_t n);


#endif
