/**
* 	primes -- C MPI parallel program to find primes very basically.
*   Author: freesec2021 at protonmail.com
*   Copyright (C) 2019 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/
#include "primes.h"
#include "util.h"


void primes2(uint64_t max_n)
{
	int rank, size;
	char * name_prefix = "out_primes2";

	uint64_t proc_n_num, proc_n_offset = 3 /* when rank == 0 */, n;
	unsigned int filename_size = 1; //when rank == 0

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	char out_filename[filename_max_size(size, name_prefix)];

	// share the workload between nodes and cores
	proc_n_num = (max_n-2) / size;
	if(rank > 0)
		proc_n_offset = proc_n_num*rank;
	// confide extra numbers to the last core
	// (needed if the remainder of (max_n - 1) / size is not zero)
	// N.B.: it's possible that last core is also first core (if size == 1)
	if(rank == size-1)
		proc_n_num += (max_n-2) % size;
	n = proc_n_offset;
	gen_filename(rank, name_prefix, out_filename);
	printf("out_filename=%s, rank=%d\n", out_filename, rank);
	printf("start_n=%d rank=%d\n", n, rank);
	max_n = (int)fmin(proc_n_num+proc_n_offset, max_n);
	FILE* out_fd = fopen(out_filename, "w");
	if(!rank)
		fprintf(out_fd, "2\n"); // 2 is a singular prime
	while(n < max_n)
	{
		if(is_prime(n))
			fprintf(out_fd, "%llu\n", n);
		n++;
	}
	fclose(out_fd);
	MPI_Barrier(MPI_COMM_WORLD);
	if(!rank) printf("Packing all primes in %s\n", name_prefix);
	cat_out_primes(rank, size, name_prefix);
}
