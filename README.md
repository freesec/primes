
This small program finds prime numbers by testing basically all possible dividers. The program is parallelized using MPI. Several methods are available (read the file primes.h for more information).

Compilation
===========

You just need to run make in the project directory. But first, Check your MPI installation and where to find the C headers. Defaultly the Makefile assumes the headers are located here /usr/include/openmpi-x86_64/, that's most likely not the case for your installation. You can specify any other location for headers with the next command:

	CFLAGS="-I /my/mpi/headers/location" make

Note that the compilation is not defaultly optimized with the appropriate flag (e.g. -O2). You might add it the same way as before with a command like this one:

	CFLAGS="-I /my/mpi/headers/location -O2" make

Running
=======

Before running you may have to verify your LD environment (or any other linking tool) is able to find MPI libraries, for example with ld I need to set the environment with this command:

	export LD_LIBRARY_PATH=/usr/lib64/openmpi/lib

Below is the usage description of the program.

	./primes [<max_n> [<method_index>]]
	The program will search all the primes from 2 to max_n
	There are 5 methods available, check the header primes.h for more information.
	By default, the method used is the first (primes1) and max_n value is 100000


**MPI Example:**

	mpiexec -n 4 primes 1000000 3

For the most part of the methods, files are created with the prime numbers found into it (they are packed in a file out_primes<method_index>).




