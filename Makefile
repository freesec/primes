# 	primes -- C MPI parallel program to find primes very basically.
#   Author: freesec2021 at protonmail.com
#   Copyright (C) 2019 freesec (pseudonym)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

LDFLAGS+=-L/usr/lib64/openmpi/lib -lmpi -lm # -lgmp
CFLAGS+= -I /usr/include/openmpi-x86_64/

primes: $(subst .c,.o,$(wildcard primes*.c)) util.o
	$(CC)  $(CFLAGS) $(LDFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@
