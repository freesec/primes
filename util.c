/**
* 	primes -- C MPI parallel program to find primes very basically.
*   Author: freesec2021 at protonmail.com
*   Copyright (C) 2019 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/
#include "util.h"
#include <stdio.h>
#include <string.h>
#include <math.h>

void cat_out_primes(unsigned int rank, unsigned int size, const char* basename)
{
	FILE* in_fd;
	char in_filename[filename_max_size(size, basename)];
	unsigned int n;
	if(!rank)
	{
		FILE * out_fd = fopen(basename, "w");
		int ret;
		for(int i=0;i < size; i++)
		{
			gen_filename(i, basename, in_filename);
			printf("in_filename=%s\n", in_filename);
			in_fd = fopen(in_filename, "r");
			while((ret=fscanf(in_fd, "%u", &n)) == 1)
			{
				fprintf(out_fd, "%u\n", n);
			}
			fclose(in_fd);
		}
		fclose(out_fd);	
	}
}

void gen_filename(unsigned int rank, const char* fname_prefix, char * out_filename)
{
	unsigned int filename_size;
	if(rank > 0)
		filename_size = (int)log10((double)rank)+1;
	else
		filename_size = 1;

	filename_size += strlen(fname_prefix)+2;
	//printf("filename_size=%d\n", filename_size);
	sprintf(out_filename, "%s.%d", fname_prefix, rank);
	out_filename[filename_size-1] = '\0';
}

size_t filename_max_size(unsigned int max_suffix_num, const char* fname_prefix)
{
	return strlen(fname_prefix)+1 /* NUL BYTE */+(size_t)(log10(max_suffix_num)+1) /* num of digits in suffix */ + 1 /* extra char: e.g. a point */;
}

void write_erathostenes_primes(const char* filepath, unsigned int rank, unsigned long int * primes_masks_llu, unsigned int num_blocks, unsigned int n_max)
{
	if(rank == 0)
	{
		FILE* fd = fopen(filepath, "w");
		for(size_t i=0;i<num_blocks;i++)
		{
			for(size_t j=(!i?2:0);j<64&&j+64*i<n_max;j++)
			{
				if(! (primes_masks_llu[i] & (1llu << j)))
					fprintf(fd, "%llu\n", j+64*i);
			}
		}
		fclose(fd);
	}
}	

