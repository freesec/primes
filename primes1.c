/**
* 	primes -- C MPI parallel program to find primes very basically.
*   Author: freesec2021 at protonmail.com
*   Copyright (C) 2019 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/
#include "primes.h"
void primes1(const uint64_t max_n)
{
	int rank, size;
	uint64_t proc_n_num, proc_n_offset = 3, n;
	
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	// share the workload between nodes and cores
	proc_n_num = (max_n - 2)/ size; // number of integers to check
					// is (max_n - 3 + 1)
	if(rank > 0)
		proc_n_offset = proc_n_num*(rank);
	// confide extra numbers to the last core
	// (needed if the remainder of (max_n - 1) / size is not zero)
	// N.B.: it's possible that last core is also first core (if size == 1)
	if(rank == size-1)
		proc_n_num += (max_n-2) % size;
	n = proc_n_offset;
	printf("proc_n_offset=%llu rank%d\n", proc_n_offset, rank);
	printf("proc_n_num=%llu rank=%d\n", proc_n_num, rank);
	if(!rank)
		printf("2\n"); // 2 is a singular prime
	while(n < proc_n_num + proc_n_offset)
	{
		if(is_prime(n))
			printf("%llu\n", n);
		n++;
	}
}
